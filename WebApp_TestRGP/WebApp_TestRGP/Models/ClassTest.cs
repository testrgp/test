﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp_TestRGP.Models;

namespace WebApp_TestRGP.Models
{
    public class ClassTest
    {
        public long IDClass;
        public string NameClass;
        public List<AttributeTest> Attributes;


        public ClassTest()
        {
            Attributes = new List<AttributeTest>();
            
        }
    }
}