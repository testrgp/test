﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp_TestRGP.Models
{
    public class ObjectTest
    {
        public long IDObject;
        public long IDClass;
        public long IDObjectParent;
        public string NameObject;
        public List<ObjectAttributeTest> ObjectAttributes;

        public ObjectTest()
        {
            ObjectAttributes = new List<ObjectAttributeTest>();

        }

    }
}