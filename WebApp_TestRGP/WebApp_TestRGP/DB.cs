﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WebApp_TestRGP.Models;
using System.Collections;

namespace WebApp_TestRGP
{
    public class DB
    {
        private SqlConnection conn;

        public DB()
        {
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings[0].ConnectionString);
                conn.Open();
            }
            catch(System.Data.SqlClient.SqlException ex)
            {

            }

            
        }

        //Выбор всех классов
        public ArrayList getAllClass()
        {
            ArrayList al = new ArrayList();
            ClassTest cl;

            SqlDataReader reader = null;

            String sql = "select ID_CLASS, NAME_CLASS from TBLCLASS";
            SqlCommand sqlComm = new SqlCommand(sql, conn);

            reader = sqlComm.ExecuteReader();
            while (reader.Read())
            {
                cl = new ClassTest();
                cl.IDClass = reader.GetInt32(0);
                cl.NameClass = reader.GetString(1);
                al.Add(cl);
            }
            reader.Close();

            for(int i = 0; i < al.Count; i++)
            {
                cl = (ClassTest)al[i];
                sql = "select ID_ATTRIBUTE, NAME_ATTRIBUTE, ID_DATATYPE from TBLATTRIBUTE where ID_CLASS = " + cl.IDClass.ToString();
                //sqlComm.Parameters.Clear();
                sqlComm = new SqlCommand(sql, conn);
                reader = null;
                reader = sqlComm.ExecuteReader();
                while (reader.Read())
                {
                    AttributeTest atr = new AttributeTest();
                    atr.IDAttribute = reader.GetInt32(0);
                    atr.NameAttribute = reader.GetString(1);
                    atr.DataType = reader.GetInt32(2).ToString();

                    cl.Attributes.Add(atr);
                }
                reader.Close();
            }   
            return al;
        }

        //Выбор выбраннного по ID класса      
        public ClassTest getClassById(long id)
        {
            ClassTest cl = new ClassTest();
            
            SqlDataReader reader = null;

            String sql = "select ID_CLASS, NAME_CLASS from TBLCLASS where ID_CLASS = " + id.ToString();
            SqlCommand sqlComm = new SqlCommand(sql, conn);

            reader = sqlComm.ExecuteReader();
            if(reader.Read())
            {
                cl.IDClass = reader.GetInt32(0);
                cl.NameClass = reader.GetString(1);


                reader.Close();

                sql = "select ID_ATTRIBUTE, NAME_ATTRIBUTE, ID_DATATYPE from TBLATTRIBUTE where ID_CLASS = " + id.ToString();
                sqlComm = new SqlCommand(sql, conn);
                reader = null;
                reader = sqlComm.ExecuteReader();

                while (reader.Read())
                {
                    AttributeTest atr = new AttributeTest();
                    atr.IDAttribute = reader.GetInt32(0);
                    atr.NameAttribute = reader.GetString(1);
                    atr.DataType = reader.GetInt32(2).ToString();

                    cl.Attributes.Add(atr);
                }
                reader.Close();
                return cl;

            }
            else
            {
                reader.Close();
                return null;
            }
             
        }

        //Выбор всех объектов
        public ArrayList getAllObject()
        {
            ArrayList al = new ArrayList();

            ObjectTest ob;

            SqlDataReader reader = null;

            String sql = "select ID_OBJECT, ID_OBJECT_PARENT, ID_CLASS, NAME_OBJECT from TBLOBJECT";
            SqlCommand sqlComm = new SqlCommand(sql, conn);

            reader = sqlComm.ExecuteReader();
            while (reader.Read())
            {
                ob = new ObjectTest();
                ob.IDObject = Convert.ToInt32(reader["ID_OBJECT"]);
                ob.IDObjectParent = Convert.ToInt32(reader["ID_OBJECT_PARENT"]);
                ob.IDClass = Convert.ToInt32(reader["ID_CLASS"]);
                ob.NameObject = reader["NAME_OBJECT"].ToString();
                al.Add(ob);

                //resultList.Add(reader["NAME_CLASS"].ToString());


            }
            reader.Close();
            return al;
        }


        //Выбор выбраннного по ID объекта
        public ObjectTest getObjectById(long id)
        {
           ObjectTest ob;

            SqlDataReader reader = null;

            String sql = "select ID_OBJECT, ID_OBJECT_PARENT, ID_CLASS, NAME_OBJECT " +
                          "from TBLOBJECT " +
                          "where ID_OBJECT = " + id.ToString();
            SqlCommand sqlComm = new SqlCommand(sql, conn);

            reader = sqlComm.ExecuteReader();
            if (reader.Read())
            {
                ob = new ObjectTest();
                ob.IDObject = Convert.ToInt32(reader["ID_OBJECT"]);
                ob.IDObjectParent = Convert.ToInt32(reader["ID_OBJECT_PARENT"]);
                ob.IDClass = Convert.ToInt32(reader["ID_CLASS"]);
                ob.NameObject = reader["NAME_OBJECT"].ToString();

                reader.Close();

                sql = "select a.ID_ATTRIBUTE, a.NAME_ATTRIBUTE, a.ID_DATATYPE, isnull(oa.VALUE_ATTRIBUTE, '') " +
                      "from TBLOBJECTATTRIBUTE oa " +
                      "left join TBLATTRIBUTE a on a.ID_ATTRIBUTE = oa.ID_ATTRIBUTE " +
                      "where oa.ID_OBJECT = " + id.ToString();
                sqlComm = new SqlCommand(sql, conn);
                reader = null;
                reader = sqlComm.ExecuteReader();

                while (reader.Read())
                {
                    AttributeTest atr = new AttributeTest();
                    atr.IDAttribute = reader.GetInt32(0);
                    atr.NameAttribute = reader.GetString(1);
                    atr.DataType = reader.GetInt32(2).ToString();

                    ObjectAttributeTest oatr = new ObjectAttributeTest();
                    oatr.AttibuteObject = atr;
                    oatr.ValueAttribute = reader.GetString(3).ToString();

                    ob.ObjectAttributes.Add(oatr);
                }
                reader.Close();


                return ob;
            }
            else
            {
                reader.Close();
                return null;
            }
        }


        //Выбор выбраннного по ID объекта с потомками
        public ArrayList getObjectByIdWithChild(long id)
        {
           ArrayList al = new ArrayList();

            ObjectTest ob;

            SqlDataReader reader = null;

            String sql = "with ObjectWithChild(ID_OBJECT, ID_CLASS, NAME_OBJECT, ID_OBJECT_PARENT, NumLevel) " +
                         "as (" +
                         "select o1.ID_OBJECT, o1.ID_CLASS, o1.NAME_OBJECT, o1.ID_OBJECT_PARENT, 0 AS NumLevel " +
                         "from TBLOBJECT o1 " +
                         "where o1.ID_OBJECT = " + id.ToString() + " " +
                         "union all " +
                         "select o2.ID_OBJECT, o2.ID_CLASS, o2.NAME_OBJECT, o2.ID_OBJECT_PARENT, NumLevel + 1 " +
                         "from TBLOBJECT AS o2 " +
                         "inner join ObjectWithChild as owc on o2.ID_OBJECT_PARENT = owc.ID_OBJECT " +
                         ") " +
                         "select ID_OBJECT, ID_CLASS, NAME_OBJECT, ID_OBJECT_PARENT, NumLevel " +
                         "from ObjectWithChild " +
                         "order by ID_OBJECT";

            SqlCommand sqlComm = new SqlCommand(sql, conn);

            reader = sqlComm.ExecuteReader();
            while (reader.Read())
            {
                ob = new ObjectTest();
                ob.IDObject = Convert.ToInt32(reader["ID_OBJECT"]);
                ob.IDObjectParent = Convert.ToInt32(reader["ID_OBJECT_PARENT"]);
                ob.IDClass = Convert.ToInt32(reader["ID_CLASS"]);
                ob.NameObject = reader["NAME_OBJECT"].ToString();

                al.Add(ob);
            }
            reader.Close();

            return al;
        }

        //Добавление нового объекта
        public bool addNewObject(long IdClass, long IdParentObject)
        {
            String sql = "select NAME_CLASS from TBLCLASS where ID_CLASS = " + IdClass.ToString();
            SqlCommand sqlComm = new SqlCommand(sql, conn);
            SqlDataReader reader = sqlComm.ExecuteReader();
            String objname = null;
            if (reader.Read())
            {
              objname = reader["NAME_CLASS"].ToString();
            }
            
            reader.Close();

            sql = "exec CREATE_OBJECT @idclass, @idparentobject, @name, @out_msg out";
            sqlComm = new SqlCommand(sql, conn);
            sqlComm.Parameters.Add("@idclass", SqlDbType.Int);
            sqlComm.Parameters.Add("@idparentobject", SqlDbType.Int);
            sqlComm.Parameters.Add("@name", SqlDbType.NVarChar);
            sqlComm.Parameters.Add("@out_msg", SqlDbType.NVarChar);

            sqlComm.Parameters["@idclass"].Value = IdClass;
            sqlComm.Parameters["@idparentobject"].Value = IdParentObject;
            sqlComm.Parameters["@name"].Value = objname + IdClass.ToString();
            sqlComm.Parameters["@out_msg"].Direction = ParameterDirection.Output;
            sqlComm.Parameters["@out_msg"].Size = 250;

            int res = sqlComm.ExecuteNonQuery();
                        
            return Convert.ToBoolean(res);
        }


        //Потомоки заданного объекта, созданные на основе класса Насос (ID_CLASS = 4), 
        //установленные до сегодняшнего дня, с массой более 10 кг
        public ArrayList getObjectByCondition(long idObject, long idClass, string condition = "")
        {
            ArrayList al = new ArrayList();

            ObjectTest ob;

            SqlDataReader reader = null;

            String sql = "with ObjectWithChild(ID_OBJECT, ID_CLASS, ID_OBJECT_PARENT, NumLevel) " + 
                         "as ( " +
                         "select o1.ID_OBJECT, o1.ID_CLASS, o1.ID_OBJECT_PARENT, 0 AS NumLevel " +
                         "from TBLOBJECT o1 " +
                         "where o1.ID_OBJECT = "+ idObject.ToString() + 
                         "union all " +
                         "select o2.ID_OBJECT, o2.ID_CLASS, o2.ID_OBJECT_PARENT, NumLevel + 1 " +
                         "from TBLOBJECT AS o2 " +
                         "inner join ObjectWithChild as owc on o2.ID_OBJECT_PARENT = owc.ID_OBJECT " +
                         ") " +
                         "select owc1.ID_OBJECT, owc1.ID_CLASS, owc1.ID_OBJECT_PARENT, owc1.NumLevel, " +
                         "       oa1.ID_ATTRIBUTE, oa1.VALUE_ATTRIBUTE, oa2.ID_ATTRIBUTE, oa2.VALUE_ATTRIBUTE, " +
                         "       (select NAME_OBJECT from TBLOBJECT where ID_OBJECT = owc1.ID_OBJECT) NAME_OBJECT " +
                         "from ObjectWithChild owc1, " +
                         "TBLOBJECTATTRIBUTE oa1, TBLOBJECTATTRIBUTE oa2 " +
                         "where owc1.ID_CLASS = " + idClass.ToString() + " and " +
                         "      oa1.ID_OBJECT = owc1.ID_OBJECT and " +
                         "      oa2.ID_OBJECT = owc1.ID_OBJECT and " +
                         "      ((oa1.ID_ATTRIBUTE = 30 and cast(ltrim(rtrim(oa1.VALUE_ATTRIBUTE)) as int) > 10) and " +
                         "       (oa2.ID_ATTRIBUTE = 31 and " +
                         "        convert(datetime, cast(ltrim(rtrim(oa2.VALUE_ATTRIBUTE)) as datetime), 104) <= cast(convert(varchar(10), getdate(), 104) as datetime))) " +
                         "order by ID_OBJECT";

            SqlCommand sqlComm = new SqlCommand(sql, conn);

            reader = sqlComm.ExecuteReader();
            while (reader.Read())
            {
                ob = new ObjectTest();
                ob.IDObject = Convert.ToInt32(reader["ID_OBJECT"]);
                ob.IDObjectParent = Convert.ToInt32(reader["ID_OBJECT_PARENT"]);
                ob.IDClass = Convert.ToInt32(reader["ID_CLASS"]);
                ob.NameObject = reader["NAME_OBJECT"].ToString();
                al.Add(ob);
            }
            reader.Close();

            return al;
        }

    }
}