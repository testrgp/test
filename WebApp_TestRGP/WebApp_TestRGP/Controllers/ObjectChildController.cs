﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp_TestRGP.Models;
using System.Collections;
using System.Web.Script.Serialization;

namespace WebApp_TestRGP.Controllers
{
    public class ObjectChildController : ApiController
    {
        // GET: api/OjectChild
        public IEnumerable<string> Get()
        {
            return new string[] { "" };
        }

        // GET: api/OjectChild/5
        public IEnumerable<string> Get(long id)
        {
            DB db = new DB();
            ArrayList al = new ArrayList();
            al = db.getObjectByIdWithChild(id);

            string[] resstr = new string[al.Count];

            //string JSONstr = null;

            for (int i = 0; i < al.Count; i++)
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                //JSONstr += jss.Serialize(al[i]);

                resstr[i] = jss.Serialize(al[i]);
            }


            //return new string[] { JSONstr };

            return resstr;
        }

        // POST: api/OjectChild
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/OjectChild/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/OjectChild/5
        public void Delete(int id)
        {
        }
    }
}
