﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp_TestRGP.Models;
using System.Collections;
using System.Web.Script.Serialization;

namespace WebApp_TestRGP.Controllers
{
    public class ObjectController : ApiController
    {
        // GET: api/Object
        public IEnumerable<string> Get()
        {           
            DB db = new DB();
            ArrayList al = new ArrayList();
            al = db.getAllObject();

            string[] resstr = new string[al.Count];

            //string JSONstr = null;

            //Ограничение на вывод 100 элентов
            for (int i = 0; i < al.Count; i++)
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                //JSONstr += jss.Serialize(al[i]);

                resstr[i] = jss.Serialize(al[i]);
            }


            //return new string[] { JSONstr };

            return resstr;
        }

        // GET: api/Object/5
        public string Get(long id)
        {
            DB db = new DB();
            ObjectTest ob = db.getObjectById(id);

            JavaScriptSerializer jss = new JavaScriptSerializer();
            string JSONstr = jss.Serialize(ob);

            return JSONstr;
        }

        // POST: api/Object
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Object/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Object/5
        public void Delete(int id)
        {
        }
    }
}
