﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp_TestRGP.Models;
using System.Collections;
using System.Web.Script.Serialization;

namespace WebApp_TestRGP.Controllers
{
    public class AddObjectController : ApiController
    {
        // GET: api/AddObject
        public IEnumerable<string> Get()
        {
            return new string[] { "" };
        }

        // GET: api/AddObject/5
        public string Get(long id)
        {
            DB db = new DB();
            bool res = db.addNewObject(id, 0);

            JavaScriptSerializer jss = new JavaScriptSerializer();
            string JSONstr = jss.Serialize(res);

            return JSONstr;
        }

        // POST: api/AddObject
        public bool Post([FromBody]string value)
        {
            ObjectTest ob = new ObjectTest();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            ob = jss.Deserialize<ObjectTest>(value);

            DB db = new DB();
            bool res = db.addNewObject(ob.IDClass, ob.IDObjectParent);

            return res;
        }

        // PUT: api/AddObject/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/AddObject/5
        public void Delete(int id)
        {
        }
    }
}
