﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp_TestRGP.Models;
using System.Collections;
using System.Web.Script.Serialization;

namespace WebApp_TestRGP.Controllers
{
    public class ObjectConditionController : ApiController
    {
        // GET: api/ObjectCondition
        public IEnumerable<string> Get()
        {
            return new string[] { "" };
        }

        // GET: api/ObjectCondition/5
        public IEnumerable<string> Get(int id)
        {
            DB db = new DB();
            ArrayList al = new ArrayList();
            al = db.getObjectByCondition(id, 4); //idclass = 4 Насос

            string[] resstr = new string[al.Count];

            //string JSONstr = null;

            for (int i = 0; i < al.Count; i++)
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                //JSONstr += jss.Serialize(al[i]);

                resstr[i] = jss.Serialize(al[i]);
            }


            //return new string[] { JSONstr };

            return resstr;
        }

        // POST: api/ObjectCondition
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ObjectCondition/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ObjectCondition/5
        public void Delete(int id)
        {
        }
    }
}
