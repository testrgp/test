﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp_TestRGP.Models;
using System.Collections;
using System.Web.Script.Serialization;


namespace WebApp_TestRGP.Controllers
{
    public class ClassController : ApiController
    {

        public IEnumerable<string> Get()
        {
            DB db = new DB();
            ArrayList al = new ArrayList();
            al = db.getAllClass();

            string[] resstr = new string[al.Count];

            //string JSONstr = null;
            for (int i = 0; i < al.Count; i++)
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                //JSONstr += jss.Serialize(al[i]);

                resstr[i] = jss.Serialize(al[i]);
            }


            //return new string[] { JSONstr };

            return resstr;
        }


        public string Get(long id)
        {
            DB db = new DB();
            ClassTest cl = db.getClassById(id);

            JavaScriptSerializer jss = new JavaScriptSerializer();
            string JSONstr = jss.Serialize(cl);

            return JSONstr;
        }

    }
}
